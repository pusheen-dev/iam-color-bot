const Eris = require('eris');
const TOKEN = process.env.TOKEN;

const client = new Eris.CommandClient(TOKEN , { }, {
  description: 'A simple role manager made with Eris.',
  ignoreBots: true,
  ignoreSelf: true,
  prefix: '.',
  owner: '152093238449274880'
});

client.on('ready', () => {
  console.log('Bot is online.');
  client.editStatus('online', { name: '.iam <hexColor>' });
});

client.registerCommandAlias('commands', 'help');

client.registerCommand('iam', function(message, args) {
  if (!args || args.length === 0) return 'Invalid input. You need to specify color: \`.iam <hexValue>\`.';
  const color = args[0];
  const roleName = new Buffer(message.member.id).toString('base64');
  // check if role with member name already exists
  let role = message.channel.guild.roles.find(function(r) { return r.name === roleName; });
  if (!role) { // create user role and check arguments
    message.channel.guild.createRole({
      name: roleName,
      color: Number(`0x${color}`)
    })
    .then(function(createRole) {
      message.member.addRole(createRole.id, 'Added via .iam command.')
        .then(function() {
          message.channel.createMessage(`Created role \`${roleName}\` for \`${message.member.username}#${message.member.discriminator}\`.`);
        })
        .catch(function(err) {
          console.error(err);
          message.channel.createMessage(`Something went wrong while adding role. Error: \n\`\`\`js\n${err.message}\n\`\`\``);
        });
    })
    .catch(console.error);
  } else {
    role.edit({
      color: Number(`0x${color}`)
    }).then(function(r) {
      message.channel.createMessage(`Changed color for \`${roleName}\` to \`#${color}\`.`);
    })
    .catch(function(err) {
      console.error(err);
      message.channel.createMessage(`Something went wrong while editing role data. Error: \n\`\`\`js\n${err.message}\n\`\`\``);
    });
  }
}, {
  description: 'Assign a color to yourself.',
  usage: '<hexValue>'
});

client.connect();

const Discord = require('discord.js');
const commands = require('./commands.js');
const TOKEN = process.env.TOKEN;
const PREFIX = '.';

const client = new Discord.Client();

client.on('ready', () => {
  console.log('[INFO] Bot is online');
  if (!client.user.presence.game) {
    console.log('[INFO] Updated status');
    client.user.setActivity('.iam <hexColor>', { type: 'PLAYING' });
  }
});

client.on('message', message => {
  if (!message.content.startsWith(PREFIX)) return;
  if (message.author.bot) return;

  const split = message.content.split(/\s+/g);
  const cmd = split[0].slice(PREFIX.length);
  const args = split.slice(1);

  if (commands.hasOwnProperty(cmd)) {
    commands[cmd](client, message, args);
    console.log(`[INFO] Executed command: ${cmd} in ${message.guild.name} (${message.guild.id})`);
  }
});

client.login(TOKEN);

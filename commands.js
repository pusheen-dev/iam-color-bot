module.exports = {
  iam: (client, message, args) => {
    if (!args || args.length === 0)
      return message.reply(`You need to pass a valid color. Ex: \`.iam #FF00D0\``);

    const hexString = args[0].replace('#', '').toUpperCase();
    
    if (!/^(?:[0-9a-fA-F]{3}){1,2}$/g.test(hexString))
      return message.reply(`Please enter a valid hex value. Ex: \`#4389E6\`, \`#FF0\``);

    const roleName = new Buffer(message.author.id).toString('base64');
    const colorRole = message.member.roles.find(role => role.name === roleName);

    if (!colorRole) {
      message.guild.createRole({
        name: roleName,
        color: hexString,
        mentionable: false
      })
      .then(role => {
        message.member.addRole(role)
          .then(member => {
            console.log(`[INFO] Created role: "${roleName}"`);
            message.reply(`Added role \`${roleName}\` with color \`#${hexString}\`.`);
          })
          .catch(err => {
            console.error(err);
            message.reply(`An error occurred while adding the role. Error: \`${err.message}\``);
          });
      })
      .catch(err => {
        console.error(err);
        message.reply(`An error occurred while creating the role. Error: \`${err.message}\``);
      });
    } else {
      colorRole.edit({
        color: hexString
      })
      .then(() => {
        console.log(`[INFO] Edited role: "${roleName}"`);
        message.reply(`Changed color to: \`#${hexString}\``);
      })
      .catch(err => {
        console.error(err);
        message.reply(`An error occurred while editing the role. Error: \`${err.message}\``);
      });
    }
  }
};
